import PlayModule from './play'
import PlayController from './play.controller';
import PlayComponent from './play.component';
import PlayTemplate from './play.html';

describe('Play', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PlayModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PlayController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(PlayTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = PlayComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PlayTemplate);
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PlayController);
      });
  });
});
