class PlayController {
  constructor(BoardService) {
    "ngInject";
    this.BoardService = BoardService;
    this.size = 0;
  }

  $onInit(){
    this.size = this.BoardService.getBoard().size;
  }
}

export default PlayController;
