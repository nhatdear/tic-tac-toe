import template from './play.html';
import controller from './play.controller';
import './play.scss';

let playComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default playComponent;
