import angular from 'angular';
import uiRouter from 'angular-ui-router';
import playComponent from './play.component';
import boardComponent from '../../components/board/board'
let playModule = angular.module('play', [
  uiRouter,
  boardComponent.name
])

.component('play', playComponent)

export default playModule;
