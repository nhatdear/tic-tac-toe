import angular from 'angular';
import uiRouter from 'angular-ui-router';
import homeComponent from './home.component';
import CreatorFormComponent from '../../containers/creatorform/creatorform';

let homeModule = angular.module('home', [
  uiRouter,
  CreatorFormComponent.name
])

.component('home', homeComponent)

export default homeModule;
