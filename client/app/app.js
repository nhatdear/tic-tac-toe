import angular      from 'angular';
import uiRouter     from 'angular-ui-router';
import AppComponent from './app.component';
import NavigationComponent from './components/navigation/navigation';
import HomeComponent from './pages/home/home';
import PlayComponent from './pages/play/play'
import BoardService from './services/BoardService'
// import our default styles for the whole application
import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.css';

angular.module('app', [
    uiRouter,
    NavigationComponent.name,
    HomeComponent.name,
    PlayComponent.name,
])
    .config(($locationProvider, $stateProvider, $urlRouterProvider) => {
      "ngInject";

      // Define our app routing, we will keep our layout inside the app component
      // The layout route will be abstract and it will hold all of our app views
      $stateProvider
          .state('app', {
            url: '/app',
            abstract: true,
            template: '<app></app>'
          })

        // Dashboard page to contain our goats list page
          .state('app.home', {
            url: '/home',
            template: '<home></home>'
          })

        // Create route for our goat listings creator
          .state('app.play', {
            url: '/play',
            template: '<play></play>'
          });

      // Default page for the router
      $urlRouterProvider.otherwise('/app/home');
    })
    .component('app', AppComponent)
    .factory('BoardService', BoardService);;