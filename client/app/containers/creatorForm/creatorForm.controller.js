class CreatorFormController {
  constructor($state,BoardService) {
    "ngInject";

    this.$state       = $state;
    this.BoardService = BoardService;

    this.board = {};
  }

  // will handle the form submission,
  // validates the required field and then adds the goat to the service.
  // once added, we will go to the next page.
  addBoard() {
    if(!this.board.size) return alert('Size is Required');
    if (this.board.size < 3) return alert('Size must be larger than 3');
    if (this.board.size > 10) return alert('Size must be smaller than 10');
    this.BoardService.createBoard(this.board);

    // reset the form
    this.board = {};

    // go to home page, to see our entry
    this.$state.go('app.play');
  }
}

export default CreatorFormController;
