import angular from 'angular';
import uiRouter from 'angular-ui-router';
import creatorFormComponent from './creatorForm.component';

let creatorFormModule = angular.module('creatorForm', [
  uiRouter
])

.component('creatorForm', creatorFormComponent)


export default creatorFormModule;
