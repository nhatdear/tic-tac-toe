import CreatorFormModule from './creatorForm'
import CreatorFormController from './creatorForm.controller';
import CreatorFormComponent from './creatorForm.component';
import CreatorFormTemplate from './creatorForm.html';

describe('CreatorForm', () => {
  let $rootScope, makeController;

  beforeEach(window.module(CreatorFormModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new CreatorFormController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(CreatorFormTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = CreatorFormComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(CreatorFormTemplate);
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(CreatorFormController);
      });
  });
});
