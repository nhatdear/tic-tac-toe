import template from './creatorForm.html';
import controller from './creatorForm.controller';
import './creatorForm.scss';

let creatorFormComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default creatorFormComponent;
