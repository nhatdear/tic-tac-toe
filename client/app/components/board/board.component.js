import template from './board.html';
import controller from './board.controller';
import './board.scss';
let boardComponent = {
  restrict: 'E',
  bindings: {
    size : '<',
  },
  template,
  controller,
  controllerAs: 'vm'
};

export default boardComponent;
