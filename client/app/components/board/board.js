import angular from 'angular';
import uiRouter from 'angular-ui-router';
import boardComponent from './board.component';

let boardModule = angular.module('board', [
  uiRouter
])

.component('board', boardComponent)


export default boardModule;
