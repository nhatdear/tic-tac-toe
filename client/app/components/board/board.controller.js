class BoardController {

  constructor($state,BoardService) {
    "ngInject";
    this.$state = $state;
    if (!this.size || this.size == 0) {
      this.$state.go('app.home');
    } else {
      this.array = [];
      for (var i = 0; i < this.size; i++) {
        this.array.push(i);
      }
      this.BoardService = BoardService;
      this.game = BoardService.getGame(this.size);
      this.game.start();
    }
  }

  cellClick(id) {
    console.log("cell click at " + id);
    var cell = document.getElementById(id);
    if(this.game.status === "running"  && !cell.classList.contains('occupied')) {
      var result = this.game.currentState.turn;
      var next = this.BoardService.getNewState(this.game.currentState);
      var size = this.size;
      var row = parseInt(id/size);
      var col = id - size*row;
      next.board[row][col] = result;

      cell.classList.add('pre-animation');
      cell.classList.add('occupied');
      cell.classList.add(next.turn.toUpperCase());
      cell.innerHTML = result;
      setTimeout(function(){
        cell.classList.remove('pre-animation')
      },100);

      next.advanceTurn();
      next.idx = id;
      var status = this.game.advanceTo(next);
      if (status.indexOf("running") == -1) {
        this.showEndGameDialog(status);
        this.game.status = "end";
      }
    }
  }

  showEndGameDialog(result) {
    setTimeout(function(){
      alert(result);
    },500);

    var p = document.getElementById("result-p");
    p.innerHTML = result;
  }

  resetGame() {
    var cells = document.getElementsByClassName("cell");
    for (var i = 0; i < cells.length; i++) {
      var cell = cells[i];
      cell.innerHTML = "";
      cell.className = cell.className.replace( /(?:^|\s)occupied(?!\S)/g , '' );
      cell.className = cell.className.replace( /(?:^|\s)X(?!\S)/g , '' )
      cell.className = cell.className.replace( /(?:^|\s)O(?!\S)/g , '' )
    }
    var p = document.getElementById("result-p");
    p.innerHTML = "";
    this.game = this.BoardService.getGame(this.size);
    this.game.start();
  }
}

export default BoardController;
