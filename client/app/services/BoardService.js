/**
 * Created by nhatd on 1/17/2017.
 */
function BoardService() {
  "ngInject";

  // private variable to store our goats entries
  var board = {
      size: 0
    }
    ;
  /**
   * Created by nhatd on 1/17/2017.
   */
  /*
   * Represents a state in the game
   * @param old [State]: old state to intialize the new state
   */
  var State = function (old) {

    /*
     * public : the player who has the turn to player
     */
    this.turn = "";

    /*
     * public : the number of moves of the AI player
     */
    this.oMovesCount = 0;

    /*
     * public : the result of the game in this State
     */
    this.result = "still running";

    /*
     * public : the board configuration in this state
     */
    this.board = [];

    /* Begin Object Construction */
    if (typeof old !== "undefined") {
      // if the state is constructed using a copy of another state
      var len = old.board.length;
      this.board = new Array(len);
      for (var itr = 0; itr < len; itr++) {
        this.board[itr] = old.board[itr];
      }

      this.oMovesCount = old.oMovesCount;
      this.result = old.result;
      this.turn = old.turn;
    }
    /* End Object Construction */

    /*
     * public : advances the turn in a the state
     */
    this.advanceTurn = function () {
      this.turn = this.turn === "X" ? "O" : "X";
      console.log("Advance Turn : " + this.turn);
    }

    /*
     * public function that enumerates the empty cells in state
     * @return [Array]: indices of all empty cells
     */
    this.emptyCells = function () {
      var indxs = [];
      for (var i = 0; i < this.board.length; i++) {
        for (var j = 0; j < this.board.length; j++) {
          if (this.board[i][j] === "E") {
            indxs.push(i+"|"+j);
          }
        }
      }
      return indxs;
    };

    /*
     * public  function that checks if the state is a terminal state or not
     * the state result is updated to reflect the result of the game
     * @returns [Boolean]: true if it's terminal, false otherwise
     */
    this.isTerminal = function (idx) {
      if (idx == null) return false;
      var rule = 3;
      if (this.board.length == 4) rule = 4;
      else if (this.board.length > 4) rule = 5;
      var B = this.board;
      var size = Math.sqrt(this.board.length * this.board.length);
      var row = parseInt(idx / size);
      var col = idx - size * row;
      console.log(row + " " + col);
      //check rows
      for (var _col = 0; _col <= size - rule; _col++) {
        try {
          var listToCompare = [];
          for (var _rule = 0; _rule < rule; _rule++) {
            listToCompare.push(B[row][_col + _rule]);
          }
          var isTerminal = true;
          for (var count = 0; count < listToCompare.length - 1; count ++) {
            if (!(listToCompare[count] !== "E" && listToCompare[count] === listToCompare[count + 1])) {
              isTerminal = false;
            }
          }
          if (isTerminal === true) {
            this.result = B[row][col] + "-won";
            return isTerminal;
          }
        } catch(e){}

      }

      //check columns
      for (var _row = 0; _row <= size - rule; _row++) {
        try {
          var listToCompare = [];
          for (var _rule = 0; _rule < rule; _rule++) {
            listToCompare.push(B[_row + _rule][col]);
          }
          var isTerminal = true;
          for (var count = 0; count < listToCompare.length - 1; count ++) {
            if (!(listToCompare[count] !== "E" && listToCompare[count] === listToCompare[count + 1])) {
              isTerminal = false;
            }
          }
          if (isTerminal === true) {
            this.result = B[row][col] + "-won";
            return isTerminal;
          }
        } catch (e) {
        }
      }

      //check diagonals
      var listToCompare = [];

      try {
        var _row = 0;
        var _col = 0;
        if (row > col) {
          _row = row - col;
        } else {
          _col = col - row;
        }
        for (var i = 0; i < this.board.length; i++) {
          try {
            listToCompare.push(B[_row + i][_col + i]);
          } catch (e) {
          }
        }
        var countToWin = 0;
        for (var count = 0; count < listToCompare.length - 1; count ++) {
          if (!(listToCompare[count] !== "E" && listToCompare[count] === listToCompare[count + 1])) {
            countToWin = 0;
          } else {
            countToWin++;
            if (countToWin === rule - 1) {
              this.result = B[row][col] + "-won";
              return true;
            }
          }
        }
      } catch (e) {
      }

      var listToCompare = [];

      try {
        var _row = 0;
        var _col = size - 1;
        if (row > (size - col - 1)) {
          _row = row - (size - col - 1);
        } else if (row < (size - col - 1)) {
          _col = (size - col - 1) - row;
        }
        for (var i = 0; i < this.board.length; i++) {
          try {
            listToCompare.push(B[_row + i][_col - i]);
          } catch (e) {
          }
        }
        var countToWin = 0;
        for (var count = 0; count < listToCompare.length - 1; count ++) {
          if (!(listToCompare[count] !== "E" && listToCompare[count] != null && listToCompare[count] === listToCompare[count + 1])) {
            countToWin = 0;
          } else {
            countToWin++;
            if (countToWin === rule - 1) {
              this.result = B[row][col] + "-won";
              return true;
            }
          }
        }
      } catch (e) {
      }

      var available = this.emptyCells();
      if (available.length == 0) {
        //the game is draw
        this.result = "Draw"; //update the state result
        return true;
      }
      else {
        return false;
      }
    };
  };

  /*
   * Constructs a game object to be played
   * @param autoPlayer [AIPlayer] : the AI player to be play the game with
   */
  var Game = function (size) {

    // public : initialize the game current state to empty board configuration
    this.currentState = new State();

    //"E" stands for empty board cell
    this.currentState.board = [];
    for (var i = 0; i < size; i++) {
      var emptyArray = [];
      this.currentState.board.push(emptyArray);
    }
    for (var i = 0; i < size; i++) {
      for (var j = 0; j < size; j++) {
        this.currentState.board[i][j] = "E";
      }
    }
    this.currentState.turn = "X"; //X plays first

    /*
     * initialize game status to beginning
     */
    this.status = "beginning";

    /*
     * public function that advances the game to a new state
     * @param _state [State]: the new state to advance the game to
     */
    this.advanceTo = function (_state) {
      this.currentState = _state;
      if (_state.isTerminal(_state.idx)) {
        return _state.result;
        console.log("Result : " + _state.result);
      } else {
        return "still running";
      }
    };

    /*
     * starts the game
     */
    this.start = function () {
      if (this.status = "beginning") {
        //invoke advanceTo with the intial state
        this.advanceTo(this.currentState);
        this.status = "running";
      }
      console.log(this.status);
    }

  };

  return {
    // Will retrieve our board for displaying
    getBoard() {
      return board;
    },

    // Creating a new board entry based on user input.
    createBoard(_board) {
      this.getBoard().size = _board.size;
    },

    getGame(size) {
      return new Game(size);
    },

    getNewState(state) {
      return new State(state);
    }
  }
}

export default BoardService;
